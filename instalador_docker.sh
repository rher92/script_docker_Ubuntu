#Script de instalación semi-automatica de docker en ubuntu
#Creada por Roger Hernandez
#Caracas, Venezuela
#e-mail : rher_92@gmail.com
#fecha 01/08/2017
#!/bin/bash

echo "atención este instaldor solo está disponible para la versiones de ubuntu : xenial, trusty, wily"
sleep 3

#paso 1
echo "script de instalación de docker en ubuntu"
echo "paso 1: instala los certificads necesarios que se requeriran para trabajar con el Docker site luego de la descarga necesaria de los 
Docker packages"
sleep 3
apt-get install apt-transport-https ca-certificates

#paso 2
echo "paso 2: agregar el nuevo GPG key. Esta clave es necesaria para garantizar que todos Los datos se cifran al descargar los paquetes necesarios para Docker."
sleep 3
sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

#paso 3
echo "paso 3: Dependiendo de la versión de Ubuntu que tengas, deberás agregar el sitio relevante a docker.list para el gestor de paquetes apt, para que pueda detectar los paquetes de Docker desde el sitio de Docker y descargarlos en consecuencia."
sleep 3

var_uno=$(lsb_release -c | cut -c 10-)
echo $var_uno

if [ $var_uno == xenial ]
then
var_dos='deb https://apt.dockerproject.org/repo ubuntu-xenial main'
fi

if [ $var_uno == trusty ]
then
var_dos='deb https://apt.dockerproject.org/repo ubuntu-trusty main'
fi

if [ $var_uno == wily ]
then
var_dos='deb https://apt.dockerproject.org/repo ubuntu-wily main'
fi

touch /etc/apt/sources.list.d/docker.list
echo $var_dos >> /etc/apt/sources.list.d/docker.list

sleep 3

#paso 4
echo "paso 4: realizamos un apt-get update, para actualizar nustra paqueteria del sistema"
sleep 3
if [ $var_uno == xenial ]
then
apt update
else
apt-get update
fi

#paso 5
echo "paso 5: verificar que el gestor de paquetes está apuntando al repositorio correcto"
sleep 3
apt-cache policy docker-engine

#paso 6
echo "paso 6: realizamos un apt-get update, para actualizar nuestra paqueteria del sistema (otra vez)"
sleep 3
if [ $var_uno == xenial ]
then
apt update
else
apt-get update
fi

#paso 7
echo "paso 7: para ubuntu:trusty,wily y xenial, debemos instalar el linux-image-extra-* paquetes del kernel, el cual te permite utilizar el aufs storage driver (driver de almacenamiento aufs). este driver es usado por las nuevas versiones de Docker"
sleep 3
sudo apt-get install linux-image-extra-$(uname -r) linux-image-extra-virtual

#paso 8
echo "paso 8: el paso final es la instalación de docker"
if [ $var_uno == xenial ]
then
apt install docker-engine
else
apt-get install docker-engine
fi

#paso 9
echo "paso 9: version de docker"
docker version
sleep 3

#paso 10
echo "paso 10: mas información de docker "
docker info
sleep 3

#paso 11
echo "ejecutamos HelloWorld Docker container"
echo "con el comando: docker run hello-world"
docker run hello-world

